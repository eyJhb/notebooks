\documentclass[10pt,landscape]{article}
\usepackage{multicol}
\usepackage{calc}
\usepackage{ifthen}
\usepackage[landscape]{geometry}
\usepackage{hyperref}

% my includes
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{graphicx,float}
\usepackage{subfigure}
\usepackage{centernot}
\usepackage{booktabs}
\usepackage{blindtext}
\usepackage{svg}


% To make this come out properly in landscape mode, do one of the following
% 1.
%  pdflatex latexsheet.tex
%
% 2.
%  latex latexsheet.tex
%  dvips -P pdf  -t landscape latexsheet.dvi
%  ps2pdf latexsheet.ps


% If you're reading this, be prepared for confusion.  Making this was
% a learning experience for me, and it shows.  Much of the placement
% was hacked in; if you make it better, let me know...


% 2008-04
% Changed page margin code to use the geometry package. Also added code for
% conditional page margins, depending on paper size. Thanks to Uwe Ziegenhagen
% for the suggestions.

% 2006-08
% Made changes based on suggestions from Gene Cooperman. <gene at ccs.neu.edu>


% To Do:
% \listoffigures \listoftables
% \setcounter{secnumdepth}{0}

\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

% This sets page margins to .5 inch if using letter paper, and to 1cm
% if using A4 paper. (This probably isn't strictly necessary.)
% If using another size paper, use default 1cm margins.
\ifthenelse{\lengthtest { \paperwidth = 11in}}
	{ \geometry{top=.5in,left=.5in,right=.5in,bottom=.5in} }
	{\ifthenelse{ \lengthtest{ \paperwidth = 297mm}}
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
		{\geometry{top=1cm,left=1cm,right=1cm,bottom=1cm} }
	}

% Turn off header and footer
\pagestyle{empty}
 

% Redefine section commands to use less space
\makeatletter
\renewcommand{\section}{\@startsection{section}{1}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%x
                                {\normalfont\large\bfseries}}
\renewcommand{\subsection}{\@startsection{subsection}{2}{0mm}%
                                {-1explus -.5ex minus -.2ex}%
                                {0.5ex plus .2ex}%
                                {\normalfont\normalsize\bfseries}}
\renewcommand{\subsubsection}{\@startsection{subsubsection}{3}{0mm}%
                                {-1ex plus -.5ex minus -.2ex}%
                                {1ex plus .2ex}%
                                {\normalfont\small\bfseries}}
\makeatother

% Define BibTeX command
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% Don't print section numbers
\setcounter{secnumdepth}{1}


\setlength{\parindent}{0pt}
\setlength{\parskip}{0pt plus 0.5ex}


% -----------------------------------------------------------------------

\begin{document}

\begin{center}
     \Large{Stochastic Processes Cheat Sheet} \\
\end{center}

\raggedright
\footnotesize
\begin{multicols}{3}


% multicol parameters
% These lengths are set only within the two main columns
%\setlength{\columnseprule}{0.25pt}
\setlength{\premulticols}{1pt}
\setlength{\postmulticols}{1pt}
\setlength{\multicolsep}{1pt}
\setlength{\columnsep}{2pt}

% expected value
% https://en.wikipedia.org/wiki/Expected_value
\section{Expected Value}

\subsection{Other Names}
\begin{tabular}{lll}
Expected Value & Expectation & Mathematical Expectation \\
Mean & Average & First Moment \\
\end{tabular}

\paragraph{Notations} $\mu$, $E[X]$, $\mu_X$ or $\left< X \right>$ (used in physics)

\subsection{Discrete Case}
Let X be a non-negative random variable with a countable set of outcomes $x_1, x_2, \dots$ occurring with probabilities $p_1, p_2, \dots$ respectively:
$$
E[X] = \sum\limits_{i=1}^{\infty} x_ip_i
$$

Check MM3 assignments for examples.

\paragraph{Example}
of the expectation of a fair six sided die:

$$
E[X]=1\cdot {\frac {1}{6}}+2\cdot {\frac {1}{6}}+3\cdot {\frac {1}{6}}+4\cdot {\frac {1}{6}}+5\cdot {\frac {1}{6}}+6\cdot {\frac {1}{6}}=3.5.
$$

\subsection{Continuous Case}
If X is a random variable with a probability density function of $f(x)$
$$
E[X] = \int_{\mathbb{R}} xf(x) dx
$$

\subsection{Properties}
- Monotonicity If $X \leq Y$ and $E[X]$ and $E[Y]$ exists, then $E[X] \leq E[Y]$. \\
- $E[X+Y] = E[X] + E[Y]$ - Linearity. \\
- $E[k] = k$ - constant k. \\
- $E[aX] = aE[X]$ - Linearity. \\
- $E[XY] = E[X]E[Y]$ - if \textbf{independent}, otherwise $\neq$. \\

\vfill\null
\columnbreak

% Variance
\section{Variance}
Variance is the expectation of the squared deviation of a random variable from its mean.
Informally, it measures how far a set of numbers is spread out from their average value.

\paragraph{Notations} $\sigma^2$, $s^2$ or $\text{Var}(X)$.

$$
Var(X) = Cov(X, X)
$$

\begin{align}
Var(X) &= E[(X-\mu)^2] \\
&= E[(X-E[X]^2)] \\
&= E[X^2-2XE[X]+E[X]^2] \\
&= E[X^2] -2E[X]E[X]+E[X]^2 \\
&= E[X^2] - E[X]^2
\end{align}

Also remember the following is true
\begin{align}
Var(X) &= E[X^2] - E[X]^2 \\
E[X^2] &= Var(X) + E[X]^2 \\
E[X]^2 &= E[X^2] - Var(X)
\end{align}

\subsection{Discrete Case}

\begin{align}
Var(X) &= \sum\limits_{i=1}^n p_i \cdot (x_i-\mu)^2 \\
Var(X) &= \left( \sum\limits_{i=1}^n p_i x_i^2 \right) - \mu^2
\end{align}

\subsection{Continuous Case}
\begin{align}
Var(X) = \int_{\mathbb{R}} x^2 f(x) dx - \mu^2
\end{align}

\subsection{Properties}
\begin{itemize}
        \tightlist
\item Non-negativity - $Var(X)$ is non-negative because the squared is positive or 0. 
\item $Var(a) = 0$ - Constant. 
\item $Var(X + a) = Var(X)$ - Invariant to a constant. 
\item $Var(aX) = a^2 Var(X)$ - Scaled by a constant. 
\item $Var(aX + bY) = a^2 Var(X) + b^2 Var(Y) + 2ab Cov(X,Y)$ - Variance of two r.v. (positive case). 
\item $Var(aX - bY) = a^2 Var(X) + b^2 Var(Y) - 2ab Cov(X,Y)$ - Variance of two r.v. (negative case). 
\item $Var(X + Y) = Var(X) + Var(Y)$ - if \textbf{Independent} - (proven by using expected value). 
\item $Var(XY)=E[X]^2 Var(Y) + E[Y]^2 Var(X) + Var(X)Var(Y)$ - \textbf{independent}. 
\item $Var(XY)=E[X^2]E[Y^2] - E[X]^2 E[Y]^2$ - \textbf{independent}. 
\item $Var(X - Y) = Var(X + (-Y)) = Var(X) + Var(-Y) = Var(X) + Var(Y)$ - if \textbf{Independent}. 
\item $Var(-X) = Var(-1 \cdot X) = (-1)^2 Var(X) = Var(X)$ - Negative sign. 
\end{itemize}

% \vfill\null
% \columnbreak


\section{Covariance}
\begin{figure}[H]
    \centering
    % \includegraphics[width=3cm]{./spfigs/coveq}
    \subfigure[]{
    \includegraphics[width=0.09\textwidth]{spfigs/covleq.png}
    }
    \subfigure[almost not correlated]{
    \includegraphics[width=0.09\textwidth]{spfigs/coveq.png}
    }
    \subfigure[]{
    \includegraphics[width=0.09\textwidth]{spfigs/covgeq.png}
    }
\end{figure}

\paragraph{Notations} $Cov(X,Y)$, $\sigma_{XF}$ or $\sigma(X,Y)$

\subsection{Definition}
$$
Cov(X,Y) = E[(X-E[X])(Y-E[Y])] = E[XY] - E[X]E[Y]
$$

\subsection{Linear Combinations}
\begin{align}\operatorname {cov} (X,a)&=0\\
\operatorname {cov} (X,X)&=\operatorname {var} (X)\\
\operatorname {cov} (X,Y)&=\operatorname {cov} (Y,X)\\
\operatorname {cov} (aX,bY)&=ab\,\operatorname {cov} (X,Y)\\
\operatorname {cov} (X+a,Y+b)&=\operatorname {cov} (X,Y)\\
\operatorname {cov} (aX+bY,cW+dV)&=ac\,\operatorname {cov} (X,W)  +ad\,\operatorname {cov} (X,V) \\
\nonumber & +bc\,\operatorname {cov} (Y,W)  +bd\,\operatorname {cov} (Y,V)
\end{align}

\subsection{Uncorrelated + Independence}
If a two r.vs are uncorrelated, then their covariance is 0.
So if X and Y are uncorrelated, then:

$$
Cov(X,Y) = 0
$$

If two r.vs are independent, then they are also correlated.
However, if they are uncorrelated it does not necessary mean they are independent.

\begin{align}
\nonumber \text{Independent} \implies \text{Uncorrelated} \\
\nonumber \text{Uncorrelated} \centernot\implies \text{Independent}
\end{align}

\section{iid - Independent and identically distributed r.vs}
Information from here \footnote{\url{https://statisticsbyjim.com/basics/independent-identically-distributed-data/}}.

\paragraph{Independent} means that all events should be independent from each other, eg. in the form of a dice roll, etc.
\paragraph{Identically distributed} means, that the mean should stay the same, as each sample is collected, and that it should not increase/decrease as samples comes.

Keep in mind, that if you have a process $X[n] \sim \mathbb{N}(\mu, \sigma^2)$, and you have an expression like $E[X[n]X[n-k]$, then for $k=0$, they are not independent, as they are the same, and you can therefore \textbf{not} do $E[X[n]]E[X[n]]$, but rather $E[X[n]^2]$.



\section{White process/white noise}
White noise have a mean of 0, and a finite variance.
Besides this, it is assumed to be iid.

\section{ACF (Autocorrelation Function)}
Insert more from slides S05

\begin{align}
    R_{XX}(n, n+k) &= E[X(n)X(n+k)] \\
    R_{XX}(n_1, n_2) &= E[X(n_1)X(n_2)]
\end{align}

\section{WSS (Wide-Sense Stationary)}
For a process to be WSS, it its mean function is constant, and its ACF does not depend on time.

\begin{align}
    \mu_X(t)         & = \mu_X(t+\tau) = \mu_X, & \; \; \text{for all $\tau \in \mathbb{R}$}     \\
    R_{XX}(t_1, t_2) & = R_{XX}(t_1-t_2,0),     & \; \; \text{for all $t_1, t_2 \in \mathbb{R}$} \\
    E[|X(t)|^2]      & < \infty,                & \; \; \text{for all $t \in \mathbb{R}$}
\end{align}


\blindtext[5]

% force a page break to explain distributions
\pagebreak

\section{Distributions/Processes}

\subsection{Normal Distribution}
\begin{tabular}{ll}
Normal & Gaussian \\
Gauss & Laplace-Gauss \\
\end{tabular}

\begin{figure}[H]
    \centering
    \subfigure[PDF]{
    \includegraphics[width=0.15\textwidth]{spfigs/normal-pdf.png}
    }
    \subfigure[CDF]{
    \includegraphics[width=0.15\textwidth]{spfigs/normal-cdf.png}
    }
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{ll}
    \toprule
             Notation   & $\mathbb{N}(\mu, \sigma^2)$                                                   \\
             Parameters & $\mu \in \mathbb{R}$ = mean (location)                                        \\
                        & $\sigma^2 > 0$ = variance (squared scale)                                     \\
             Support    & $x \mu \mathbb{R}$                                                            \\
    \midrule PDF        & $ \frac{1}{\sigma \sqrt{2\pi}}e^{-\frac{1}{2}(\frac{x-\mu}{\sigma})^2}$       \\
             CDF        & $ \frac{1}{2} \left[ 1+erf\left(\frac{x-\mu}{\sigma \sqrt{2}}\right) \right]$ \\
    \midrule Mean       & $ \mu $                                                                       \\
             Median     & $ \mu $                                                                       \\
             Mode       & $ \mu $                                                                       \\
             Variance   & $ \sigma^2 $                                                                  \\
    \bottomrule
\end{tabular}
\end{table}


% https://en.wikipedia.org/wiki/Sum_of_normally_distributed_random_variables
\subsubsection{Adding two independent normal distributions}
\begin{align}
    X &\sim N(\mu_X,\sigma_X^2) \\
    Y &\sim N(\mu_Y,\sigma_Y^2) \\
    Z &= X + Y \\
    \text{then} \; Z &\sim N(\mu_X + \mu_Y, \sigma_X^2 + \sigma^2)
\end{align}

\subsubsection{Misc stuff}
You can change any Normal distribution, e.g. the mean/variance ($\sigma^2$), by multiplying/adding.
E.g. if you have the process $X[n] = \mathbb{N}(1,4)$, and you want to make it to a 0 mean and variance 2, then you can do $-1 + X[n]/sqrt(2)$.

An easier example is just if have have a process $Y[n] = \mathbb{N}(0,1)$ and want 0 mean and variance 2, then do $0 + sqrt(2)*Y[n]$.

\vfill\null
\columnbreak

\subsection{(Continuous) Uniform distribution}
\begin{figure}[H]
    \centering
    \subfigure[PDF]{
    \includegraphics[width=0.15\textwidth]{spfigs/uniform-pdf.png}
    }
    \subfigure[CDF]{
    \includegraphics[width=0.15\textwidth]{spfigs/uniform-cdf.png}
    }
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{ll}
    \toprule
             Notation & $U(a,b)$ or $unif(a,b)$ \\
             Parameters & $-\infty < a < b < \infty$ \\
             Support & $x \in [a,b]$ \\
    \midrule PDF & $ \left\{ \begin{matrix} \frac{1}{b-a} & \; for \; x \in [a,b] \\ 0 & \; otherwise\\ \end{matrix} \right. $ \\
             CDF & $ \left\{ \begin{matrix} 0 & \; for \; x<a \\ \frac{x-a}{b-a} & \; for \; x \in [a,b] \\ 1 & \; for \; x>b \end{matrix} \right. $ \\
    \midrule Mean & $\frac{1}{2}(a+b)$ \\
             Median & $\frac{1}{2}(a+b)$ \\
             Mode & Any value in $(a,b)$ \\
             Variance & $\frac{1}{12}(b-a)^2$ \\
    \bottomrule
\end{tabular}
\end{table}

\vfill\null
\columnbreak

\subsection{Bernoulli process}
\begin{figure}[H]
    \centering
    \subfigure[PDF]{
    \includegraphics[width=0.15\textwidth]{spfigs/binomial-pmf.png}
    }
    \subfigure[CDF]{
    \includegraphics[width=0.15\textwidth]{spfigs/binomial-cdf.png}
    }
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{ll}
    \toprule
             Parameters & $\begin{matrix} 0 \geq p \geq 1 \\ q = 1 - p \end{matrix}$ \\
             Support    & $k \in {0,1}$                                                                 \\
    \midrule PMF        & $ \left\{ \begin{matrix} q = 1 - p & \; \text{if} \; k = 0   \\ p & \; \text{if} \; k = 1 \end{matrix} \right. $ \\
                        & $ p^k(1-p)^{1-k} $                                                            \\
             CDF        & $ \frac{1}{2} \left[ 1+erf\left(\frac{x-\mu}{\sigma \sqrt{2}}\right) \right]$ \\
    \midrule Mean       & $ \mu $                                                                       \\
             Median     & $ \mu $                                                                       \\
             Mode       & $ \mu $                                                                       \\
             Variance   & $ \sigma^2 $                                                                  \\
    \bottomrule
\end{tabular}
\end{table}

\vfill\null
\columnbreak

\pagebreak

% https://www.mathsisfun.com/data/data-discrete-continuous.html
\section{Discrete vs. Continuous data}
\paragraph{Discrete} data can only take a certain value, such as a dice roll, you cannot roll 1.5
\paragraph{Continuous} data can take any value within a range, such as your height or time in a race (you can measure it to fractions of a second)


\section{Basic}
PDF = probability density function

CDF = Cumulative distribution function

\rule{0.3\linewidth}{0.25pt}
\scriptsize

Copyright \copyright\ 2020 eyJhb

\href{https://gitlab.com/eyJhb/notebooks}{https://gitlab.com/eyJhb/notebooks}


\end{multicols}
\end{document}


