# generate-directory jupyterlab-drawio @axlair/jupyterlab_vim @jupyterlab/toc jupyterlab_filetree
# with import <nixpkgs> {};
# { pkgs ? import <nixpkgs> {} }:

let

  # jupyter = import ./jupyterWith {};
  # jupyter = import (fetchFromGitHub {
  #   owner = "tweag";
  #   repo = "jupyterWith";
  #   rev = "b410ed1f6f78f1890a66eada5063731ae7eef8e5";
  #   sha256 = "0d92cz997wzasvbiyfhzfg0zqmsm641z2fivg9fr4b6r9nsrkgxg";
  # }) {};
  jupyterWithPath = builtins.fetchGit {
    url = https://github.com/tweag/jupyterWith;
    rev = "b410ed1f6f78f1890a66eada5063731ae7eef8e5";
  };

  # Importing overlays from that path.
  overlays = [
    # Only necessary for Haskell kernel
    (import "${jupyterWithPath}/nix/haskell-overlay.nix")
    # Necessary for Jupyter
    (import "${jupyterWithPath}/nix/python-overlay.nix")
    (import "${jupyterWithPath}/nix/overlay.nix")
  ];

  pkgs = import <nixpkgs> { inherit overlays; };

  iPython = pkgs.jupyterWith.kernels.iPythonWith {
    name = "python";
    packages = p: 
    let
      pyJupyterLabLatex = p.buildPythonPackage rec {
        pname = "jupyterlab-latex";
        version = "v2.0.0";

        patchPhase = ''
          sed -i "s/'notebook'//g" setup.py
          cat setup.py
        '';

        doCheck = false;
        
        src = pkgs.fetchFromGitHub {
          owner = "jupyterlab";
          repo = "jupyterlab-latex";
          rev = version;
          sha256 = "0xkzn3chhk14l20m1fpq8mkl01yaiwvhbiw3kslsc33nbi8aaznc";
        };
      };
    in
    [
      pkgs.fontconfig
      pkgs.graphviz
      p.matplotlib
      p.scipy
      p.sympy
      p.numpy
      p.networkx # graphs
      p.graphviz
      # pyJupyterLabLatex
    ];
  };

  jupyterEnvironment =
    pkgs.jupyterWith.jupyterlabWith {
      kernels = [ iPython ];
      directory = ./jupyterlab;
    };
in
  jupyterEnvironment.env
