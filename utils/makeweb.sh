#!/bin/sh

LINKPREFIX=""
OUTDIR="./public"

HTMLSTART=$(cat <<-END
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>eyJhb Jupyter Notebooks</title>
  </head>
  <body>
END
)
HTMLEND=$(cat <<-END
  </body>
</html>
END
)

# remove out dir
rm -rf $OUTDIR/

# convert files
find . -not -path '*/\.*' -name '*.ipynb' -exec jupyter nbconvert --to html {} \;

# make a index file
mkdir -p $OUTDIR
echo $HTMLSTART > $OUTDIR/index.html
find . \( -name '*.html' -o -name '*.pdf' \) -print | sort -n | xargs -i printf "<a href='%s'>%s</a><br>\n" "$LINKPREFIX{}" "{}" > $OUTDIR/index.html
echo $HTMLEND >> $OUTDIR/index.html

# move files
find . \( -name '*.html' -o -name '*.png' \) -exec sh -c 'echo {} && mkdir -p '$OUTDIR'/$(dirname {}) && cp {} '$OUTDIR'/{}' \;

# compile LaTeX
mkdir $OUTDIR/cheatsheets
cp cheatsheets/stochastics.pdf $OUTDIR/cheatsheets

# done?
