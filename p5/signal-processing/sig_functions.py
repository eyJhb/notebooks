from math import *

def butterPoles(k,n):
    return exp((((2*k-1)/(2*n))*pi + (pi/2)))